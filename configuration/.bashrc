#Environment variables
export PS1='\[\033[1;32m\]\u@\h \[\033[1;33m\]\w \[\033[36m\]$(parse_git_branch)\[\033[00m\]\n[!\!]$ '

#Functions
parse_git_branch() {
 git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

show_git_history_for_branch() {
	if [ $# -eq 0 ]
		then
			gitk&
		else
			gitk $1 &

	fi
}


#Aliases
alias sj6='export JAVA_HOME="c:\\Program Files\\Java\\jdk1.6.0_45\\";echo $JAVA_HOME'

alias cdb='cd /c/coredb-services-devel/coredb-test'
alias cbb='cd /d/Workspace/git/bitbucket'
alias cca='cd /d/Workspace/git/customer_application'
alias gs='git status'
alias gf='git fetch'
alias gb='git branch'
alias gc='git checkout'
alias gk=show_git_history_for_branch
alias gcp='git checkout src/test/resources/configuration.properties;git status'
alias gt='git log --graph --decorate --stat'
alias ggd=$'env GIT_AUTHOR_DATE=$(date +\'%F\')\' 23:59:00\' git gui'
alias gso='git remote show origin'
alias gpr='git pull --rebase'
alias gcm='git commit -m'
alias gaa='git add .;git status'
alias gacm='gaa;gcm'

alias mci='mvn clean install'